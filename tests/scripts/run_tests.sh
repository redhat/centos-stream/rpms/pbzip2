#!/bin/bash
set -eux

# Test basic pbzip2 operations

# Just some text we know is present
cat /usr/share/doc/pbzip2/* > test-file.txt
sha256sum test-file.txt > test-file.txt.sha256

# Compress a stream from stdin
pbzip2 -p4 < test-file.txt > test-stream.bz2
[ -e test-stream.bz2 ] || exit 1

# Uncompress and check it
pbzip2 -p4 -dc < test-stream.bz2 > test-file.txt
sha256sum -c test-file.txt.sha256

# Compress a file (removes original)
pbzip2 -p4 test-file.txt
[ -e test-file.txt.bz2 ] || exit 1

# Uncompress and check it
pbzip2 -p4 -df test-file.txt.bz2
sha256sum -c test-file.txt.sha256

echo "PASS"
exit 0
